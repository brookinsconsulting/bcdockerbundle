<?php

/*
 * This file is part of the docker-bundle-example package.
 *
 * (c) Daniel Ribeiro <drgomesp@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BrookinsConsulting\BcDockerBundle\Compose\Service\Traits;

use BrookinsConsulting\BcDockerBundle\Compose\ServiceInterface;

/**
 * Class VolumesFromAwareTrait
 *
 * @author Daniel Ribeiro <drgomesp@gmail.com>
 * @package BrookinsConsulting\BcDockerBundle\Compose\Service
 */
trait VolumesFromAwareTrait
{
    /**
     * @var ServiceInterface[]
     */
    protected $originVolumes;

    /**
     * @param ServiceInterface $service
     * @return self
     */
    public function addOriginVolume(ServiceInterface $service)
    {
        $this->originVolumes[] = $service;
        return $this;
    }

    /**
     * @return \BrookinsConsulting\BcDockerBundle\Compose\ServiceInterface[]
     */
    public function getOriginVolumes()
    {
        return $this->originVolumes;
    }

    /**
     * @param array $serialized
     */
    public function applyOriginVolumes(array &$serialized)
    {
        $serialized[$this->getName()]['volumes_from'] = array_map(function (ServiceInterface $service) {
            return $service->getName();
        }, $this->originVolumes);
    }
}
