<?php

/*
 * This file is part of the docker-bundle-example package.
 *
 * (c) Daniel Ribeiro <drgomesp@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BrookinsConsulting\BcDockerBundle\Compose\Service;

use BrookinsConsulting\BcDockerBundle\Compose\Service\Composition\VolumesAwareInterface;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Traits\ServiceTrait;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Traits\VolumesAwareTrait;
use BrookinsConsulting\BcDockerBundle\Compose\ServiceInterface;
use BrookinsConsulting\BcDockerBundle\Yaml\Traits\YamlConvertibleTrait;
use BrookinsConsulting\BcDockerBundle\Yaml\YamlConvertibleInterface;

/**
 * Represents the application service which contains the code.
 *
 * @author Daniel Ribeiro <daniel.ribeiro@propertyfinder.ae>
 * @package BrookinsConsulting\BcDockerBundle\Compose\Service
 */
class App extends AbstractService implements VolumesAwareInterface, YamlConvertibleInterface
{
    use VolumesAwareTrait, YamlConvertibleTrait;

    protected function applyDeeperJsonSerialize(array &$serialized)
    {
        $this->applyVolumes($serialized);
    }
}
