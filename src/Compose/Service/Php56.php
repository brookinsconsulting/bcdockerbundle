<?php

/*
 * This file is part of the docker-bundle-example package.
 *
 * (c) Daniel Ribeiro <drgomesp@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BrookinsConsulting\BcDockerBundle\Compose\Service;

use BrookinsConsulting\BcDockerBundle\Compose\Service\Composition\VolumesFromAwareInterface;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Traits\ServiceTrait;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Traits\VolumesFromAwareTrait;
use BrookinsConsulting\BcDockerBundle\Compose\ServiceInterface;
use BrookinsConsulting\BcDockerBundle\Yaml\Traits\YamlConvertibleTrait;
use BrookinsConsulting\BcDockerBundle\Yaml\YamlConvertibleInterface;

/**
 * Represents the php 5.6 service that contains php and php-fpm.
 *
 * @author Daniel Ribeiro <daniel.ribeiro@propertyfinder.ae>
 * @package BrookinsConsulting\BcDockerBundle\Compose\Service
 */
class Php56 extends AbstractService implements VolumesFromAwareInterface, YamlConvertibleInterface
{
    use VolumesFromAwareTrait, YamlConvertibleTrait;

    /**
     * {@inheritdoc}
     */
    protected function applyDeeperJsonSerialize(array &$serialized)
    {
        $this->applyOriginVolumes($serialized);
    }
}
