<?php

/*
 * This file is part of the docker-bundle-example package.
 *
 * (c) Daniel Ribeiro <drgomesp@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BrookinsConsulting\BcDockerBundle\Compose\Service;

use BrookinsConsulting\BcDockerBundle\Compose\Service\Composition\EnvironmentAwareInterface;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Composition\PortsAwareInterface;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Composition\VolumesFromAwareInterface;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Traits\EnvironmentAwareTrait;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Traits\PortsAwareTrait;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Traits\ServiceTrait;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Traits\VolumesAwareTrait;
use BrookinsConsulting\BcDockerBundle\Compose\Service\Traits\VolumesFromAwareTrait;
use BrookinsConsulting\BcDockerBundle\Compose\ServiceInterface;
use BrookinsConsulting\BcDockerBundle\Yaml\Traits\YamlConvertibleTrait;
use BrookinsConsulting\BcDockerBundle\Yaml\YamlConvertibleInterface;

/**
 * Represents the nginx web server.
 *
 * @author Daniel Ribeiro <daniel.ribeiro@propertyfinder.ae>
 * @package BrookinsConsulting\BcDockerBundle\Compose\Service
 */
class Nginx extends AbstractService implements
    PortsAwareInterface,
    EnvironmentAwareInterface,
    VolumesFromAwareInterface,
    YamlConvertibleInterface
{
    use PortsAwareTrait, EnvironmentAwareTrait, VolumesFromAwareTrait, YamlConvertibleTrait;

    /**
     * {@inheritdoc}
     */
    protected function applyDeeperJsonSerialize(array &$serialized)
    {
        $this->applyEnvironment($serialized);
        $this->applyPorts($serialized);
        $this->applyOriginVolumes($serialized);
    }
}
